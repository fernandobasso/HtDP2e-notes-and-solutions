#lang htdp/bsl

(define (==> sunny friday)
  (or (not sunny) friday))

(==> #f #t)
